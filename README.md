# mail
Source: https://bobcares.com/blog/clamav-postfix/
Sourcw: https://www.linux.com/training-tutorials/using-clamav-kill-viruses-postfix/

1. apt-get install clamav clamsmtp clamav-freshclam

2. Next, we edit the configuration file /etc/clamsmtpd.conf. Here, we update the below lines from

OutAddress: 10025
Listen: 1xx.x.x.1:10026
to

OutAddress: 10026
Listen: 1xx.x.x.1:10025

3. Then, we edit the postfix configuration file /etc/postfix/main.cf and add the below contents

content_filter = scan:1xx.x.x.1:10025
receive_override_options = no_address_mappings
4. Also, we edit the /etc/postfix/master.cf file and add the below contents

# AV scan filter (used by content_filter)
scan unix - - n - 16 smtp
-o smtp_send_xforward_command=yes
# For injecting mail back into postfix from the filter
1xx.x.x.1:10026 inet n - n - 16 smtpd
-o content_filter=
-o receive_override_options=no_unknown_recipient_checks,no_header_body_checks
-o smtpd_helo_restrictions=
-o smtpd_client_restrictions=
-o smtpd_sender_restrictions=
-o smtpd_recipient_restrictions=permit_mynetworks,reject
-o mynetworks_style=host
-o smtpd_authorized_xforward_hosts=1xx.x.x.0/8
5. Lastly, we restart postfix, clamAV-freshclam, and clamsmtp.

